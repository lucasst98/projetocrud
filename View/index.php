<?php
    include_once '../Controller/connection.php';
?>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../Style/login.css">
        <title>Login</title>
    </head>
    <body>
        <div id="center">
            <div>
                <form action="../Controller/loginController.php" method="POST">
                    <div>
                        <label for="email">Email</label><br>
                        <input type="email" id="email" name="email" placeholder="Exemplo@gmail.com"><br>
                    </div>
                    <div>
                        <label for="password">Senha</label><br>
                        <input type="password" name="password" id="password" placeholder="Senha">
                    </div>
                    <div class="button">
                        <input type="submit" value="entrar" id="entrar" name="entrar">
                        <a href="../View/register.php">Cadastrar</a>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>