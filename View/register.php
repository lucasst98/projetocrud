<?php
    include_once '../Controller/connection.php';
    
?>
<head>
    <link rel="stylesheet" href="../Style/register.css">
    <link rel="stylesheet" href="../Style/login.css">
    <title>Cadastro</title>
</head>
<body>
    <div id="center">
        <div>
            <form action="../Controller/insertController.php" method="POST">
                <input type="hidden" name="method" value="insert">
                <div>
                    <label for="name">Nome:</label><br>
                    <input type="name" name="name" id="name" placeholder="Nome">
                </div>
                <div>
                    <label for="email">Email:</label><br>
                    <input type="email" id="email" name="email" placeholder="Exemplo@gmail.com"><br>
                </div>
                <div>
                    <label for="password">Senha:</label><br>
                    <input type="password" name="password" id="password" required placeholder="Senha">
                </div>
                <div>
                    <label for="telephone">Telefone:</label><br>
                    <input type="telephone" name="telephone" id="telephone" placeholder="(xx)x-xxxx.xxxx" >
                </div>
                <div class="button">
                    <input type="submit" value="Cadastrar" >
                </div>
            </form>
        </div>
    </div>
</body>
