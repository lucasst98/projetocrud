<?php
    include_once '../Controller/connection.php';
    include '../Controller/listController.php';    
?>
<head>
    <link rel="stylesheet" href="../Style/edit.css">

    <title>Cadastro</title>
</head>
<body>
    <div id="center">
        <div id="body">
            <form action="../Controller/editController.php" method="GET">
                <table>
                <?php
                    foreach ($result as $r) {
                        $id = $r['id'];
                        echo "<tr>";
                            // echo "<input type='hidden' value='".$r['id']."' name='id'>";
                            echo "<td>"."<input class='select' value='".$r['name']."' name='name'>"."</td>";
                            echo "<td>"."<input class='select' value='".$r['email']."' name='email'>"."</td>";
                            echo "<td>"."<input class='select' value='".$r['telephone']."' name='telephone'>"."</td>";
                            echo "<td>"."<button type='submit' value='".$id."' name='id'>Editar</button>"."</td>";
                        echo "</tr>";
                    }                            
                ?>
                <a href='../View/list.php'>voltar</a> -
                <a href='../View/index.php'>Sair</a>
                </table>
            </form>
        </div>
    </div>
</body>
